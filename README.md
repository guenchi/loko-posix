# POSIX API for Loko Scheme

The aim of this package is to be compatible with [scsh][scsh], with
support for as much as possible. It should allow for an implementation
of SRFI 170 to be built on top of it.

 [scsh]: https://scsh.net/docu/html/man-Z-H-4.html

This is early work.
